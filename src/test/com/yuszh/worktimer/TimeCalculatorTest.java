package com.yuszh.worktimer;

import static org.junit.Assert.*;

import org.joda.time.LocalTime;
import org.junit.Test;

public class TimeCalculatorTest {

	@Test
	public void testLeavingTime() {

		TimeCalculator timeCalculator = new TimeCalculator();
		LocalTime leavingTime = timeCalculator.calculateLeavingTime(8, 30, 8,
				42, 8, 42);

		assertEquals(17, leavingTime.getHourOfDay());
		assertEquals(12, leavingTime.getMinuteOfHour());

	}

}
