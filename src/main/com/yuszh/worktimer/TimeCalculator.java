package com.yuszh.worktimer;

import org.joda.time.LocalTime;

public class TimeCalculator {

	public LocalTime calculateLeavingTime(int startHour, int startMinute,
			int defaultWorkHour, int defaultWorkMinute, int expectedWorkHour,
			int expectedWorkMinute) {
			
		 LocalTime startTime = new LocalTime(startHour, startMinute);
		 
		 return startTime.plusHours(expectedWorkHour).plusMinutes(expectedWorkMinute);

	}

}
