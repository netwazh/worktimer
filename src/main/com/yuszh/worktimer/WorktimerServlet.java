package com.yuszh.worktimer;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.LocalTime;

@SuppressWarnings("serial")
public class WorktimerServlet extends HttpServlet {

	private TimeCalculator timeCalculator;

	public void init() throws ServletException {
		timeCalculator = new TimeCalculator();
	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		String startHour = req.getParameter("startHour");
		String startMinute = req.getParameter("startMinute");

		if (startHour != null && startMinute != null) {

			try {

				LocalTime calculateLeavingTime = timeCalculator
						.calculateLeavingTime(Integer.parseInt(startHour),
								Integer.parseInt(startMinute), 8, 42, 8, 42);
				resp.setContentType("application/json");

				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.append("{\"leavingHour\" : \"")
						.append(calculateLeavingTime.getHourOfDay())
						.append("\" , \"leavingMinute\" : \"")
						.append(calculateLeavingTime.getMinuteOfHour())
						.append("\"}");

				resp.getWriter().println(stringBuilder.toString());
			} catch (NumberFormatException e) {
				System.err.println(e);
			}

		}else{
			resp.getWriter().println("Failed!");
		}

	}
}
