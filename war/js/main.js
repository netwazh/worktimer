$(document).ready(function(){
    $("#calculatBtn").click(function(){
    	var sh = $('#startHour').val();
    	var sm = $('#startMinute').val();
        $.getJSON("timer", { startHour:sh , startMinute:sm}, function(result){
        	$("#leavingTime").html("<span class='glyphicon glyphicon-log-out'></span>  You can leave at <strong>" + result.leavingHour + " : " + result.leavingMinute + "</strong> today.");
        });
        
    });
});